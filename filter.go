package math

import "errors"

// FilterInPlace applies a kernel at point p of grid g. Kernel dimensions should be odd numbers.
func FilterInPlace(kernel, g [][]float64, p Vector2[int]) error {
	var err error
	g[p.Y][p.X], err = FilterAtPoint(kernel, g, p)
	return err
}

// Same as FilterInPlace but returns the pixel value without applying it.
func FilterAtPoint(kernel, g [][]float64, p Vector2[int]) (float64, error) {
	if len(kernel) < 1 {
		return 0, errors.New("bad grid")
	}

	kx := len(kernel[0])
	ky := len(kernel)

	if kx%2 == 0 || ky%2 == 0 {
		return 0, errors.New("even kernel size")
	}

	avg := 0.0
	for i := -ky / 2; i <= ky/2; i++ {
		for j := -kx / 2; j <= kx/2; j++ {
			idx := p.Add(Vector2[int]{j, i})
			gv := 0.0
			box := Box2D[int]{}.New(0, 0, len(g[0]), len(g))
			if box.Contains(idx) {
				gv = g[idx.Y][idx.X]
			}
			avg += kernel[i+ky/2][j+kx/2] * gv
		}
	}
	return avg, nil
}
