package math

import "image"

type Box2D[N Number] struct {
	P1, P2 Vector2[N]
}

func NewBox2D[N Number](p1x, p1y, p2x, p2y N) Box2D[N] {
	return Box2D[N]{
		P1: Vector2[N]{X: p1x, Y: p1y},
		P2: Vector2[N]{X: p2x, Y: p2y},
	}
}

func (b Box2D[N]) New(p1x, p1y, p2x, p2y N) Box2D[N] {
	b.P1.X, b.P1.Y, b.P2.X, b.P2.Y = p1x, p1y, p2x, p2y
	return b
}

func (b Box2D[N]) FromImageRect(r image.Rectangle) Box2D[N] {
	b.P1.X, b.P1.Y, b.P2.X, b.P2.Y = N(r.Min.X), N(r.Min.Y), N(r.Max.X), N(r.Max.Y)
	return b
}

// Returns complementary point P3 which if the box is canonical is the upper-left one.
func (b Box2D[N]) P3() Vector2[N] {
	return Vector2[N]{b.P1.X, b.P2.Y}
}

// Returns complementary point P4 which if the box is canonical is the lower-right one.
func (b Box2D[N]) P4() Vector2[N] {
	return Vector2[N]{b.P2.X, b.P1.Y}
}

func (b Box2D[N]) ToImageRect() image.Rectangle {
	return image.Rect(int(b.P1.X), int(b.P1.Y), int(b.P2.X), int(b.P2.Y))
}

func (b Box2D[N]) IsBox() bool {
	return b.P1.X != b.P2.X && b.P1.Y != b.P2.Y
}

// Is v in the box (upper bounds inclusive).
func (b Box2D[N]) ContainsInc(v Vector2[N]) bool {
	return v.X >= b.P1.X && v.Y >= b.P1.Y && v.X <= b.P2.X && v.Y <= b.P2.Y
}

// Is v in the box (upper bounds non inclusive).
func (b Box2D[N]) Contains(v Vector2[N]) bool {
	return v.X >= b.P1.X && v.Y >= b.P1.Y && v.X < b.P2.X && v.Y < b.P2.Y
}

// Is box ob inside this box (upper bounds non inclusive).
func (b Box2D[N]) ContainsBox(ob Box2D[N]) bool {
	return b.Contains(ob.P1) && b.Contains(ob.P2)
}

// Is box ob inside this box (Upper bounds inclusive).
func (b Box2D[N]) ContainsBoxInc(ob Box2D[N]) bool {
	return b.ContainsInc(ob.P1) && b.ContainsInc(ob.P2)
}

// Make P1 min(x,y) and P2 max(x,y)
func (b *Box2D[N]) MakeCanonical() {
	if b.P1.X > b.P2.X {
		b.P1.X, b.P2.X = b.P2.X, b.P1.X
	}
	if b.P1.Y > b.P2.Y {
		b.P1.Y, b.P2.Y = b.P2.Y, b.P1.Y
	}
}

// Force vector inside the box
func (b Box2D[N]) Fit(v Vector2[N]) Vector2[N] {
	return Vector2[N]{
		Min(Max(v.X, b.P1.X), b.P2.X),
		Min(Max(v.Y, b.P1.Y), b.P2.Y),
	}
}

// CropToFitIn box to fit into other box. Boxes must overlap partially.
func (b *Box2D[N]) CropToFitIn(other Box2D[N]) {

	b.MakeCanonical()
	other.MakeCanonical()

	if b.P1.X < other.P1.X {
		b.P1.X = other.P1.X
	}
	if b.P1.Y < other.P1.Y {
		b.P1.Y = other.P1.Y
	}
	if b.P2.X > other.P2.X {
		b.P2.X = other.P2.X
	}
	if b.P2.Y > other.P2.Y {
		b.P2.Y = other.P2.Y
	}
}

// Do the boxes overlap at all
func (b Box2D[N]) Overlaps(other Box2D[N]) bool {
	return b.P1.X < other.P2.X &&
		b.P2.X > other.P1.X &&
		b.P1.Y < other.P2.Y &&
		b.P2.Y > other.P1.Y
}

// Size returns the box's width and height
func (b Box2D[N]) Size() Vector2[N] {
	return Vector2[N]{
		Abs(b.P2.X - b.P1.X),
		Abs(b.P2.Y - b.P1.Y),
	}
}

// Segment an int box into (size)-sized smaller boxes.
func SegmentBox(box Box2D[int], size int) []Box2D[int] {

	if !box.IsBox() || size <= 0 {
		return nil
	}

	box.MakeCanonical() //make p1<p2

	xsize := box.P2.X - box.P1.X + 1
	ysize := box.P2.Y - box.P1.Y + 1

	// find partition factors for each dimension
	for xsize*ysize > size {
		xdiv := Divisor(xsize)
		ydiv := Divisor(ysize)
		// could not split
		if xdiv == 1 && ydiv == 1 {
			return nil
		}

		// segment along the longest side to keep boxes close to square
		if (xsize/xdiv > ysize/ydiv && xdiv != 1) || ydiv == 1 {
			xsize = xsize / xdiv
		} else {
			ysize = ysize / ydiv
		}
	}

	boxes := []Box2D[int]{}

	for y := box.P1.Y; y < box.P2.Y; y += ysize {
		for x := box.P1.X; x < box.P2.X; x += xsize {
			minibox := Box2D[int]{
				P1: Vector2[int]{x, y},
				P2: Vector2[int]{x + xsize - 1, y + ysize - 1},
			}
			boxes = append(boxes, minibox)
		}
	}

	return boxes
}
