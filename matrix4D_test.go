package math

import "testing"

func TestMatrix4fMulPoint3f(t *testing.T) {

	m := Matrix4[float32]{}
	m.Populate([16]float32{
		1, 0, 0, 2,
		0, 1, 0, -3,
		0, 0, 1, 4,
		0, 0, 0, 1,
	})
	p := Vector3[float32]{1, 2, 3}
	newp := m.MulVector(p)
	pn := Vector3[float32]{3, -1, 7}
	if newp != pn {
		t.Error(newp, pn)
	}

	m.Translation(Vector3[float32]{1, 2, -1})
	p = Vector3[float32]{1, 2, 3}
	newp = m.MulVector(p)
	pn = Vector3[float32]{2, 4, 2}
	if newp != pn {
		t.Error(newp, pn)
	}

	m.Scale(Vector3[float32]{1, 2, 3})
	p = Vector3[float32]{1, 2, 3}
	newp = m.MulVector(p)
	pn = Vector3[float32]{1, 4, 9}
	if newp != pn {
		t.Error(newp, pn)
	}
}

func TestMatrix4fMulMatrix(t *testing.T) {
	ma, mb := Matrix4[float32]{}, Matrix4[float32]{}

	for i := range ma.Elements {
		ma.Elements[i] = float32(i)
	}

	mb.SetIdentity()

	result := ma.MulMatrix(mb)

	for i := range ma.Elements {
		if ma.Elements[i] != result.Elements[i] {
			t.Error(result)
		}
	}
}

func TestMatrix4Tranform(t *testing.T) {
	m := NewMatrix4Transform[float32](Vector3[float32]{1, 0, 0}, Vector3[float32]{2, 2, 1}, 0, 0, 0.7853982)
	v := m.MulVector(Vector3[float32]{0.5, 0.5, 0})

	if !v.Equals(Vector3[float32]{1, 1.4142, 0}, EpsilonLax) {
		t.Error(v)
	}
}

func TestMatrix4Decompose(t *testing.T) {
	translate := Vector3[float32]{22, 5, -3}
	scale := Vector3[float32]{2, 3, 4}
	//rotation := Vector3[float32]{0, 0, 1.5}

	tmat := NewMatrix4Translation(translate)
	rmatX := NewMatrix4RotateZ(float32(1))
	rmatY := NewMatrix4RotateZ(float32(-2))
	rmatZ := NewMatrix4RotateZ(float32(1.5))
	rmat := rmatX.MulMatrix(rmatY).MulMatrix(rmatZ)
	smat := NewMatrix4Scale(scale)
	mat := (tmat.MulMatrix(rmat)).MulMatrix(smat)

	tr, ro, sc := mat.Decompose()
	if !tr.Equals(translate, EpsilonLax) {
		t.Error(tr)
	}
	if !sc.Equals(scale, EpsilonLax) {
		t.Error(sc, mat)
	}
	t.Log(ro)
}
