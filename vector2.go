// Point in 2D (integer)

package math

import (
	"math"
)

type Vector2[N Number] struct{ X, Y N }

func (v Vector2[N]) Zero() Vector2[N] {
	return Vector2[N]{0, 0}
}

func (v Vector2[N]) Add(b Vector2[N]) Vector2[N] {
	return Vector2[N]{v.X + b.X, v.Y + b.Y}
}

func (v Vector2[N]) Sub(b Vector2[N]) Vector2[N] {
	return Vector2[N]{v.X - b.X, v.Y - b.Y}
}

func (v Vector2[N]) Mul(b Vector2[N]) Vector2[N] {
	return Vector2[N]{v.X * b.X, v.Y * b.Y}
}

func (v Vector2[N]) Div(b Vector2[N]) Vector2[N] {
	return Vector2[N]{v.X / b.X, v.Y / b.Y}
}

func (v Vector2[N]) Dot(b Vector2[N]) N {
	return v.X*b.X + v.Y*b.Y
}

func (v Vector2[N]) Scale(b N) Vector2[N] {
	return Vector2[N]{v.X * b, v.Y * b}
}

func (v Vector2[N]) Length() N {
	return N(math.Sqrt(float64(v.X*v.X) + float64(v.Y*v.Y)))
}

func (v Vector2[N]) Equals(b Vector2[N], e N) bool {
	return Equals(v.X, b.X, e) && Equals(v.Y, b.Y, e)
}

// Normalize returns a unit vector with the same direction as v. Will not work on integers
func (v Vector2[N]) Normalize() Vector2[N] {
	length := v.Length()
	return Vector2[N]{v.X / length, v.Y / length}
}

func (v Vector2[N]) AddZ(z N) Vector3[N] {
	return Vector3[N]{v.X, v.Y, z}
}

// Linearly interpolate a to b by t
func (a Vector2[N]) Lerp(b Vector2[N], t N) Vector2[N] {
	return a.Add(b.Sub(a).Scale(t))
}

// Perform a type cast
func Vector2ConvertType[A Number, B Number](a Vector2[A]) Vector2[B] {
	return Vector2[B]{B(a.X), B(a.Y)}
}
