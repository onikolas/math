package math

import (
	"math"
)

// Distance between two points.
func DistancePointPoint[N Number](p1, p2 Vector2[N]) N {
	return p1.Sub(p2).Length()
}

// Is the distance between points a and b <= r. Avoids calling distance.
func InRange[N Number](a, b Vector2[N], r N) bool {
	d := a.Sub(b)
	return d.Dot(d) <= r*r
}

// Point-line d. Line defined by points l1, l2.
func DistancePointLine(l1, l2, p Vector2[float64]) float64 {
	dy := l2.Y - l1.Y
	dx := l2.X - l1.X
	t := (dy*p.X - dx*p.Y + l2.X*l1.Y - l2.Y*l1.X) / math.Sqrt(dy*dy+dx*dx)
	if t > 0 {
		return t
	} else {
		return -t
	}
}

// Distance of point p flom line segment l1-l2.
func DistancePointLineSeg(l1, l2, p Vector2[float64]) float64 {

	// Project p-l1 to ls (l1-l2) and normalize to [0,1]
	pl1 := Vector2[float64]{p.X - l1.X, p.Y - l1.Y}
	ls := Vector2[float64]{l2.X - l1.X, l2.Y - l1.Y}
	mls := DistancePointPoint(l1, l2)
	if Equals(mls, 0, Epsilon) {
		return DistancePointPoint(p, l1)
	}
	proj := pl1.Dot(ls) / (mls * mls)

	// 'behind' the line segment
	if proj < 0 {
		return DistancePointPoint(p, l1)
	}
	// 'in front' of the line segment
	if proj > 1 {
		return DistancePointPoint(p, l2)
	}
	// next to the line segment
	return DistancePointLine(l1, l2, p)
}

// Returns the vector perpendicular to line l1-l2 from p.
func VectorPointLine[N Number](l1, l2, p Vector2[N]) Vector2[N] {
	D := l2.Sub(l1)
	D = D.Normalize()
	PA := p.Sub(l1)
	d := PA.Dot(D)
	return Vector2[N]{l1.X + d*D.X - p.X, l1.Y + d*D.Y - p.Y}
}

// Evaluates an ellipse centered at cx,cy with focal points a,b at parameter t.
func Ellipse(a, b, cx, cy, t float64) Vector2[float64] {
	return Vector2[float64]{
		cx + a*math.Cos(t),
		cy + b*math.Sin(t),
	}
}
