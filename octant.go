package math

import "math"

// Vector to octant
// 3 2 1
// 4   0
// 5 6 7
func VectorToOctant(a Vector2[float64]) int {
	angie := math.Atan2(a.Y, a.X)
	return int(8*angie/(2*math.Pi)+8.5) % 8
}

// Octant to pixel offset
// 3 2 1    -1, 1  0, 1  1, 1
// 4   0 -> -1, 0        1, 0
// 5 6 7    -1,-1  0,-1  1,-1
func OctantToOffset(oct int) Vector2[int] {
	switch oct {
	case 0:
		return Vector2[int]{1, 0}
	case 1:
		return Vector2[int]{1, 1}
	case 2:
		return Vector2[int]{0, 1}
	case 3:
		return Vector2[int]{-1, 1}
	case 4:
		return Vector2[int]{-1, 0}
	case 5:
		return Vector2[int]{-1, -1}
	case 6:
		return Vector2[int]{0, -1}
	case 7:
		return Vector2[int]{1, -1}
	default:
		return Vector2[int]{0, 0}
	}
}
