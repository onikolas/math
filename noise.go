package math

import "math/rand"
import "errors"

type Noise1D struct {
	d []float64
}

// Return an initialized noise array
func MakeNoise(size, window int) Noise1D {
	n := Noise1D{}
	n.Initialize(size, window)
	return n
}

// Initialize a 1D noise array
func (n *Noise1D) Initialize(size, window int) {
	if window == 0 {
		return
	}
	if size%window != 0 {
		size += size % window
	}
	n.d = make([]float64, size)

	nvectors := size/window + 1
	vecs := make([]float64, nvectors)
	for i := range vecs {
		vecs[i] = rand.Float64()*2 - 1
	}
	vecs[len(vecs)-1] = vecs[0] // so that the signal loops

	for i := range n.d {
		v0 := i / window
		v1 := (v0 + 1) % nvectors
		li := (0.5 + float64(i%window)) / float64(window)
		n.d[i] = Lerp(vecs[v0], vecs[v1], Fade(li))
	}
}

// Get value at point t[0,1). Values > 1 are wrapped. Values < 0 are ignored.
func (n Noise1D) Sample(t float64) float64 {
	if t < 0 {
		return 0
	}
	it := int(t)
	t = t - float64(it)
	return n.d[int(t*float64(len(n.d)))]
}

// Scale a noise array by a factor
func (n *Noise1D) Scale(a float64) {
	for i := range n.d {
		n.d[i] = n.d[i] * a
	}
}

// Combine two noise arrays with matching sizes.
func (n *Noise1D) Combine(m *Noise1D) error {
	if len(n.d) != len(m.d) {
		return errors.New("size mismatch")
	}
	for i := range n.d {
		n.d[i] += m.d[i]
	}
	return nil
}
