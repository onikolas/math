package math

import "testing"

func TestChoseRandom(t *testing.T) {

	options := []Category{
		{300, 0.8},
		{100, 0.1},
		{200, 0.1},
	}

	ArrangeCategories(options)

	type test struct {
		r  float64
		id int
	}

	cases := []test{
		{0.0, 100},
		{0.05, 100},
		{0.15, 200},
		{0.3, 300},
		{9.0, 0},
		{-1, 100},
	}

	for _, val := range cases {
		cat := ChoseRandom(options, val.r)
		if cat.Id != val.id {
			t.Error(cat, val)
		}
	}

	/*
		c300 := 0
		c200 := 0
		c100 := 0
		for i := 0; i < 10000; i++ {
			c := ChoseRandom(options, rand.Float64())
			if c.Id == 100 {
				c100++
			}
			if c.Id == 200 {
				c200++
			}
			if c.Id == 300 {
				c300++
			}
		}

		fmt.Println("c100: ", c100, "c200: ", c200, "c300: ", c300)
	*/

}

func TestSample(t *testing.T) {
	dist := []float64{0.1, 0.6, 0.3}
	samples := []int{0, 0, 0}

	n := 1000

	for i := 0; i < n; i++ {
		s, err := Sample(dist, 100)
		if err != nil {
			t.Error(err)
		}
		samples[s]++
	}

	if samples[0] > samples[1] || samples[0] > samples[2] || samples[2] > samples[1] {
		t.Error("bad sample frequencies ", samples)
	}

	if samples[0]+samples[1]+samples[2] != n {
		t.Error(samples[0] + samples[1] + samples[2])
	}

	t.Log(samples)
}

func TestSample2D(t *testing.T) {
	dist := [][]float64{
		{0.1, 0.4, 0.1},
		{0.2, 0.0, 0.2},
	}
	samples := [][]int{
		{0, 0, 0},
		{0, 0, 0},
	}

	n := 1000

	for i := 0; i < n; i++ {
		sx, sy, err := Sample2D(dist, 100)
		if err != nil {
			t.Error(err)
		}
		samples[sy][sx]++
	}

	// todo: add more checks
	if samples[0][0] > samples[0][1] {
		t.Error("bad sample frequencies ", samples)
	}

	sum := 0
	for _, v := range samples {
		for _, u := range v {
			sum += u
		}
	}
	if sum != n {
		t.Error(samples)
	}

	t.Log(samples)
}

func TestHmapToHist(t *testing.T) {
	hmap := [][]int{
		{1, 2, 3},
		{4, 2, 4},
		{1, 2, 3},
	}
	hist := MapHistogram(hmap)
	counts := []int{0, 2, 3, 2, 2}

	for i, v := range hist {
		if v != counts[i] {
			t.Error(i, v, counts[i])
		}
	}
}

func TestNormalizeDist(t *testing.T) {
	d := []int{1, 5, 6, 2, 2}
	nd := NormalizeHist(d)
	sum := 0.0
	for _, v := range nd {
		sum += v
	}
	if !Equals(sum, 1.0, 0.0001) {
		t.Error(sum)
	}
	t.Log(nd)
}

func TestNtable(t *testing.T) {
	hmap := [][]int{
		{1, 2, 3},
		{4, 2, 4},
		{1, 2, 3},
	}
	ntable := NTable(hmap)
	for i := range ntable {
		t.Log(i, ntable[i])
	}
}
