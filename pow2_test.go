package math

import (
	"testing"
)

func TestNextPowerOf2(t *testing.T) {
	tests := []struct{ a, b uint64 }{
		{0, 1},
		{1, 1},
		{2, 2},
		{3, 4},
		{5, 8},
		{6, 8},
		{11, 16},
		{22, 32},
		{63, 64},
		{72, 128},
		{222, 256},
		{301, 512},
		{700, 1024},
		{1911, 2048},
		{3562, 4096},
		{3562 * 2, 4096 * 2},
		{3561 * 4, 4096 * 4},
		{3562 * 8, 4096 * 8},
		{3562 * 16, 4096 * 16},
	}

	for i := range tests {
		if p, e := tests[i].b, NextPowerOf2(tests[i].a); p != e {
			t.Error(i, p, e)
		}
	}
}
