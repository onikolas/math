package math

import (
	"errors"
	"math/rand"
	"sort"
)

type Category struct {
	Id     int
	Weight float64
}

type ByWeight []Category

// ByWeight implements the sorting interface
func (a ByWeight) Len() int           { return len(a) }
func (a ByWeight) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByWeight) Less(i, j int) bool { return a[i].Weight < a[j].Weight }

// Sort by weight and make weights cumulative and normal
func ArrangeCategories(entities []Category) {

	sort.Sort(ByWeight(entities))
	sumWeight := 0.0
	for i, cat := range entities {
		sumWeight += cat.Weight
		// make cumulative
		entities[i].Weight = sumWeight
	}

	if sumWeight < Epsilon {
		sumWeight = 1
	}

	// normalize
	for i := range entities {
		entities[i].Weight = entities[i].Weight / sumWeight
	}

}

// Randomly choose some entry in categories given a random value r in [0, 1)
// Assumes entities are sorted and weights are cumulative (use Arrange)
func ChoseRandom(entities []Category, r float64) Category {

	for _, cat := range entities {
		if r < cat.Weight {
			return cat
		}
	}

	return Category{}
}

// Sample distribution d using rejection sampling. Returns an error if number of rejections exceeds
// rejections parameter. No checks on the validity (i.e adds up to 1) of d are made.
func Sample(d []float64, rejections int) (int, error) {
	xsize := len(d)
	for i := 0; i < rejections; i++ {
		u := rand.Intn(xsize)
		if d[u] >= rand.Float64() {
			return u, nil
		}
	}
	return 0, errors.New("max rejections exceeded")
}

// Sample a 2D distribution using rejection sampling.
func Sample2D(d [][]float64, rejections int) (int, int, error) {
	ysize := len(d)
	xsize := len(d[0])

	for i := 0; i < rejections; i++ {
		u := rand.Intn(ysize)
		v := rand.Intn(xsize)
		if d[u][v] >= rand.Float64() {
			return v, u, nil
		}
	}

	return 0, 0, errors.New("max rejections exceeded")
}

// MapHistogram computes the histogram of an int heightmap.
func MapHistogram(hmap [][]int) []int {
	max := 0
	for i := range hmap {
		for j := range hmap[i] {
			if hmap[i][j] > max {
				max = hmap[i][j]
			}
		}
	}
	hist := make([]int, max+1)

	for i := range hmap {
		for j := range hmap[i] {
			val := hmap[i][j]
			hist[val]++
		}
	}
	return hist
}

// Normalize distribution so that it adds up to 1.
func NormalizeHist(d []int) []float64 {
	nd := make([]float64, len(d))
	sum := 0
	for _, v := range d {
		sum += v
	}
	for i, v := range d {
		nd[i] = float64(v) / float64(sum)
	}
	return nd
}

// Computes the neighbourhood table (ntable) of a hightmap. The ntable contains a histogram of
// neighbour frequencies for each height in the heightmap. It uses a 4-neighbourhood.
// Note: ntable is implemented as a 2D array which is ok for small height values but can be very
// inefficient if heights are large (it allocates max(height)^2). It should be reimplemented as
// a map if that is the case.
// TODO: bins
func NTable(hmap [][]int) [][]float64 {
	max := 0
	for i := range hmap {
		for j := range hmap[i] {
			if hmap[i][j] > max {
				max = hmap[i][j]
			}
		}
	}

	ntable := make([][]int, max+1)
	for i := range ntable {
		ntable[i] = make([]int, max+1)
	}

	for i := range hmap {
		for j := range hmap[i] {
			current_height := hmap[i][j]
			for k := 0; k <= 6; k += 2 {
				point := Vector2[int]{j, i}.Add(OctantToOffset(k))
				b := Box2D[int]{}.New(0, 0, len(hmap[i]), len(hmap))
				if b.Contains(point) {
					neigh_height := hmap[point.Y][point.X]
					ntable[current_height][neigh_height]++
				}
			}
		}
	}

	ntable_norm := make([][]float64, max+1)

	for i := range ntable {
		ntable_norm[i] = NormalizeHist(ntable[i])
	}

	return ntable_norm
}
