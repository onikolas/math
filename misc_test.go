package math

import (
	"testing"
)

func TestDoubleGauss(t *testing.T) {
	tests := [][]float64{
		{0.0, 0, 1, 0.3989422804},
		{0, -2, 0.5, 0.00026766045},
		{1, 3, 0.99, 0.05236607617},
	}

	for i := range tests {

		d := Gauss(tests[i][0], tests[i][1], tests[i][2])
		if !Equals(d, tests[i][3], 0.01) {
			t.Error(tests[i], d)
		}
	}
}

func TestDivisor(t *testing.T) {
	tests := []Vector2[int]{
		{3, 3},
		{15, 3},
		{20, 2},
		{26, 2},
		{5, 1},
	}

	for i := range tests {
		if Divisor(tests[i].X) != tests[i].Y {
			t.Error(tests[i].X, "!=", tests[i].Y)
		}
	}
}

func TestFade(t *testing.T) {
	prev := -1.0
	for i := 0.0; i < 1; i += 0.1 {
		if Fade(i) < prev {
			t.Error(Fade(i), prev)
		}
		t.Log(i, Fade(i))
		prev = Fade(i)
	}
}

func TestClockwise(t *testing.T) {
	tests := []struct {
		a, b Vector2[float64]
		yes  bool
	}{
		{Vector2[float64]{1, 1}, Vector2[float64]{1, 0}, false},
		{Vector2[float64]{0, 1}, Vector2[float64]{1, 0}, false},
		{Vector2[float64]{0, 1}, Vector2[float64]{-1, 0}, true},
		{Vector2[float64]{-3, 1}, Vector2[float64]{1, 0}, false},
		{Vector2[float64]{-3, -3}, Vector2[float64]{-1, 0}, false},
		{Vector2[float64]{-3, -3}, Vector2[float64]{0, -2}, true},
	}

	for _, v := range tests {
		if Clockwise(v.a, v.b) != v.yes {
			t.Error(v)
		}
	}
}

func TestClamp(t *testing.T) {
	tests := []struct {
		x, min, max, res int
	}{
		{1, 2, 3, 2},
		{-1, 2, 3, 2},
		{-1, -5, 3, -1},
		{-1, -5, 3, -1},
		{-1, -5, -3, -3},
		{7, -5, 6, 6},
	}

	for _, test := range tests {
		if r := Clamp(test.x, test.min, test.max); r != test.res {
			t.Error(r, test)
		}
	}
}
