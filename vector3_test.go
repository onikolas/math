package math

import "testing"

var e = 0.000001
var ei = 0

func TestVector3Zero(t *testing.T) {
	a := Vector3[int]{}.Zero()
	if !a.Equals(Vector3[int]{0, 0, 0}, ei) {
		t.Error()
	}
}

func TestVector3Equals(t *testing.T) {
	a, b := Vector3[int]{1, 2, -4}, Vector3[int]{1, 2, -4}
	if !a.Equals(b, ei) {
		t.Error()
	}
	c, d := Vector3[float32]{1.11, 2.0001, -4.3}, Vector3[float32]{1.11, 2.0001, -4.3}
	if !c.Equals(d, float32(e)) {
		t.Error()
	}
	c, d = Vector3[float32]{1.11, 2.0001, -4.31}, Vector3[float32]{1.11, 2.0001, -4.3}
	if c.Equals(d, float32(e)) {
		t.Error()
	}
}

func TestVector3Arithmetic(t *testing.T) {
	a, b := Vector3[int]{1, 2, -4}, Vector3[int]{1, 2, -4}
	if !a.Add(b).Equals(Vector3[int]{2, 4, -8}, ei) {
		t.Error(a.Add(b))
	}
	c, d := Vector3[float32]{1.11, 2.0001, -4.3}, Vector3[float32]{1.11, 2.0001, -4.3}
	if !c.Add(d).Equals(Vector3[float32]{2.22, 4.0002, -8.6}, float32(e)) {
		t.Error(c.Add(d))
	}
	g, f := Vector3[float64]{1.11, 2.0001, -4.3}, Vector3[float64]{1.0, 2.0, -4.0}
	if !g.Sub(f).Equals(Vector3[float64]{0.11, 0.0001, -0.3}, e) {
		t.Error(g.Sub(f))
	}
	g, f = Vector3[float64]{1.11, 2.0001, -4.3}, Vector3[float64]{1.0, 2.0, -4.0}
	if !g.Mul(f).Equals(Vector3[float64]{1.11, 4.0002, 17.2}, e) {
		t.Error(g.Mul(f))
	}
	g, f = Vector3[float64]{1.11, 2.0001, -4.3}, Vector3[float64]{1.0, 2.0, -4.0}
	if !g.Div(f).Equals(Vector3[float64]{1.11, 1.00005, 1.075}, e) {
		t.Error(g.Mul(f))
	}
	if !Equals(g.Dot(f), 1.11+2.0001*2+4.3*4, e) {
		t.Error(g.Dot(f))
	}
}

func TestVector3Normalize(t *testing.T) {
	a := Vector3[float64]{77., 999.1, -1234.56}
	if !Equals(a.Normalize().Length(), 1, e) {
		t.Error(a.Normalize(), a.Normalize().Length())
	}
	b := Vector3[int]{77, 0, 0}
	if !Equals(b.Normalize().Length(), 1, ei) {
		t.Error(b.Normalize(), b.Length())
	}
	c := Vector3[int]{77, 11, 11}
	if !Equals(c.Normalize().Length(), 0, ei) {
		t.Error(c.Normalize(), c.Length())
	}
}

func TestVector3Lerp(t *testing.T) {
	a := Vector3[float32]{1, 2, 3}
	b := Vector3[float32]{2, 2, -1}

	c := a.Lerp(b, 0.5)
	if !c.Equals(Vector3[float32]{1.5, 2, 1}, Epsilon) {
		t.Error(a, b, c)
	}
}
