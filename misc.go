package math

import (
	"math"

	"golang.org/x/exp/constraints"
)

// Gaussian of x
func Gauss[F constraints.Float](x, mean, sigma F) F {
	d, m, s := float64(x), float64(mean), float64(sigma)
	return F(math.Exp(-((d-m)*(d-m))/(2*s*s)) / math.Sqrt(2*math.Pi*s))
}

// 8-pack
func Abs[N Number](n N) N {
	if n < 0 {
		return -n
	}
	return n
}

func Divisor[N constraints.Integer](n N) N {
	if n%2 == 0 {
		return 2
	}
	if n%3 == 0 {
		return 3
	}

	s := N(math.Sqrt(float64(n)))
	if n%s == 0 {
		return s
	}
	return 1 //we got an optimus prime
}

func Lerp(a, b, t float64) float64 {
	return a + (b-a)*t
}

func Fade(t float64) float64 {
	return t * t * t * (t*(t*6-15) + 10)
}

// Make sure x is in the range [min,max]
func Clamp[N Number](x, min, max N) N {
	return Min(max, Max(x, min))
}

// Check if vector a is clockwise of vector b. Only works for angles up to Pi.
// (all vectors are clockwise if you want it bad enough).
func Clockwise(a, b Vector2[float64]) bool {
	// normal
	na := Vector2[float64]{-a.Y, a.X}

	// size of b's projection on na
	if na.Dot(b) < 0 {
		return false
	}
	return true
}
