package math

// Next power of 2 after v (http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2).
func NextPowerOf2(v uint64) uint64 {
	v--
	v |= v >> 1
	v |= v >> 2
	v |= v >> 4
	v |= v >> 8
	v |= v >> 16
	v |= v >> 32
	v++
	if v == 0 {
		v++
	}
	return v
}

// 0101 v=5
// 0100 --
// 0010 >> 2
// 0110 |=
// 0011 >> 4
// 0111 |=
// 0011 >>
// 0111 |= ...
// 1000 ++

// 011 v=3
// 010 --
// 001 10 >> 1
// 011 10 |= 01
// ...
// 100 ++
