package math

import (
	"fmt"
	"math"
)

type Matrix4[N Number] struct {
	Elements [16]N
}

func NewMatrix4Identity[N Number]() Matrix4[N] {
	m := Matrix4[N]{}
	m.SetIdentity()
	return m
}

func NewMatrix4Translation[N Number](v Vector3[N]) Matrix4[N] {
	m := Matrix4[N]{}
	m.Translation(v)
	return m
}

func NewMatrix4Scale[N Number](v Vector3[N]) Matrix4[N] {
	m := Matrix4[N]{}
	m.Scale(v)
	return m
}

func NewMatrix4RotateX[N Number](angle N) Matrix4[N] {
	m := Matrix4[N]{}
	m.RotationX(angle)
	return m
}

func NewMatrix4RotateY[N Number](angle N) Matrix4[N] {
	m := Matrix4[N]{}
	m.RotationY(angle)
	return m
}

func NewMatrix4RotateZ[N Number](angle N) Matrix4[N] {
	m := Matrix4[N]{}
	m.RotationZ(angle)
	return m
}

// Create a transformation matrix from a translation, XYZ rotations and scale
func NewMatrix4Transform[N Number](translation, scale Vector3[N], rotationX, rotationY, rotationZ N) Matrix4[N] {
	t := NewMatrix4Translation(translation)
	s := NewMatrix4Scale(scale)
	rx := NewMatrix4RotateX(rotationX)
	ry := NewMatrix4RotateY(rotationY)
	rz := NewMatrix4RotateZ(rotationZ)
	r := rx.MulMatrix(ry).MulMatrix(rz)
	transform := (t.MulMatrix(r)).MulMatrix(s)
	return transform
}

// Set matrix elements - data must be in rows.
func (m *Matrix4[N]) Populate(data [16]N) {
	copy(m.Elements[:], data[:])
}

// Matrix becomes identity matrix
func (m *Matrix4[N]) SetIdentity() {
	m.Elements = [16]N{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	}
}

// Make m a translation matrix
func (m *Matrix4[N]) Translation(v Vector3[N]) {
	m.Elements = [16]N{
		1, 0, 0, v.X,
		0, 1, 0, v.Y,
		0, 0, 1, v.Z,
		0, 0, 0, 1,
	}
}

// Make a scaling matrix
func (m *Matrix4[N]) Scale(v Vector3[N]) {
	m.Elements = [16]N{
		v.X, 0, 0, 0,
		0, v.Y, 0, 0,
		0, 0, v.Z, 0,
		0, 0, 0, 1,
	}
}

// Set to rotation matrix around X
func (m *Matrix4[N]) RotationX(radians N) {
	c := N(math.Cos(float64(radians)))
	s := N(math.Sin(float64(radians)))
	m.Elements = [16]N{
		1, 0, 0, 0,
		0, c, -s, 0,
		0, s, c, 0,
		0, 0, 0, 1,
	}
}

// Set to rotation matrix around Y
func (m *Matrix4[N]) RotationY(radians N) {
	c := N(math.Cos(float64(radians)))
	s := N(math.Sin(float64(radians)))
	m.Elements = [16]N{
		c, 0, s, 0,
		0, 1, 0, 0,
		-s, 0, c, 0,
		0, 0, 0, 1,
	}
}

// Set to rotation matrix around Z
func (m *Matrix4[N]) RotationZ(radians N) {
	c := N(math.Cos(float64(radians)))
	s := N(math.Sin(float64(radians)))
	m.Elements = [16]N{
		c, -s, 0, 0,
		s, c, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	}
}

// Set to rotation around axis (unit vector)
func (m *Matrix4[N]) RotationAxis(axis Vector3[N], angle float64) {
	c := N(math.Cos(angle))
	t := N(1 - math.Cos(angle))
	s := N(math.Sin(angle))

	x, y, z := axis.X, axis.Y, axis.Z

	m.Elements = [16]N{
		t*x*x + c, t*x*y - s*z, t*x*z + s*y, 0,
		t*x*y + s*z, t*y*y + c, t*y*z - s*x, 0,
		t*x*z - s*y, t*y*z + s*x, t*z*z + c, 0,
		0, 0, 0, 1,
	}
}

// Matrix x vector multiplication
func (m Matrix4[N]) MulVector(p Vector3[N]) Vector3[N] {
	e := m.Elements
	return Vector3[N]{
		X: e[0]*p.X + e[1]*p.Y + e[2]*p.Z + e[3],
		Y: e[4]*p.X + e[5]*p.Y + e[6]*p.Z + e[7],
		Z: e[8]*p.X + e[9]*p.Y + e[10]*p.Z + e[11],
	}
}

func (ml Matrix4[N]) MulMatrix(mr Matrix4[N]) Matrix4[N] {
	result := Matrix4[N]{}
	result.Elements = [16]N{
		dot4(ml.Row(0), mr.Column(0)), dot4(ml.Row(0), mr.Column(1)), dot4(ml.Row(0), mr.Column(2)), dot4(ml.Row(0), mr.Column(3)),
		dot4(ml.Row(1), mr.Column(0)), dot4(ml.Row(1), mr.Column(1)), dot4(ml.Row(1), mr.Column(2)), dot4(ml.Row(1), mr.Column(3)),
		dot4(ml.Row(2), mr.Column(0)), dot4(ml.Row(2), mr.Column(1)), dot4(ml.Row(2), mr.Column(2)), dot4(ml.Row(2), mr.Column(3)),
		dot4(ml.Row(3), mr.Column(0)), dot4(ml.Row(3), mr.Column(1)), dot4(ml.Row(3), mr.Column(2)), dot4(ml.Row(3), mr.Column(3)),
	}
	return result
}

// Returns the i-th row
func (m Matrix4[N]) Row(i int) []N {
	return m.Elements[i*4 : i*4+4]
}

// Returns the i-th column
func (m Matrix4[N]) Column(i int) []N {
	return []N{
		m.Elements[i],
		m.Elements[i+4],
		m.Elements[i+8],
		m.Elements[i+12],
	}
}

func (m Matrix4[N]) String() string {
	return fmt.Sprint(
		"\n",
		m.Elements[0:4], "\n",
		m.Elements[4:8], "\n",
		m.Elements[8:12], "\n",
		m.Elements[12:16], "\n",
	)
}

// Return the translation, rotation and scale vectors that make up this matrix.
// TODO rotation
func (m Matrix4[N]) Decompose() (translation, rotation, scale Vector3[N]) {
	translation.X = m.Elements[3]
	translation.Y = m.Elements[7]
	translation.Z = m.Elements[11]

	scale.X = Vector3[N]{m.Elements[0], m.Elements[4], m.Elements[8]}.Length()
	scale.Y = Vector3[N]{m.Elements[1], m.Elements[5], m.Elements[9]}.Length()
	scale.Z = Vector3[N]{m.Elements[2], m.Elements[6], m.Elements[10]}.Length()

	return
}

// helper for matrix multiplication
func dot4[N Number](a, b []N) N {
	return a[0]*b[0] + a[1]*b[1] + a[2]*b[2] + a[3]*b[3]
}
