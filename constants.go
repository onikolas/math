package math

const (
	EpsilonVeryLax    = 0.1
	EpsilonLax        = 0.001
	Epsilon           = 0.000001
	EpsilonStrict     = 0.000000001
	EpsilonVeryStrict = 0.000000000001
)
