package math

import "testing"

func TestBoxIsBox(t *testing.T) {
	b := Box2D[int]{Vector2[int]{2, 3}, Vector2[int]{4, 5}}
	if !b.IsBox() {
		t.Error("it is")
	}

	b = Box2D[int]{Vector2[int]{2, 3}, Vector2[int]{4, 3}}
	if b.IsBox() {
		t.Error("it is not")
	}

	b = Box2D[int]{Vector2[int]{2, 3}, Vector2[int]{2, 7}}
	if b.IsBox() {
		t.Error("it is not")
	}
}

func TestBoxCanonical(t *testing.T) {
	b := Box2D[int]{Vector2[int]{2, 1}, Vector2[int]{1, 2}}
	b.MakeCanonical()

	if b.P1.X != 1 || b.P2.X != 2 {
		t.Error("did not swap Xs")
	}

	if b.P1.Y != 1 || b.P2.Y != 2 {
		t.Error("swapped Ys")
	}
}

func TestBoxCrop(t *testing.T) {

	a := Box2D[int]{Vector2[int]{1, 1}, Vector2[int]{5, 5}}
	b := Box2D[int]{Vector2[int]{0, 0}, Vector2[int]{6, 8}}
	b.CropToFitIn(a)
	if a != b {
		t.Error("a!=b", a, b)
	}

	b = Box2D[int]{Vector2[int]{-1, -1}, Vector2[int]{3, 3}}
	b.CropToFitIn(a)
	if a.P1 != b.P1 {
		t.Error("a.P1 != b.P1", a, b)
	}
	if b.P2 != (Vector2[int]{3, 3}) {
		t.Error("b.P2 =", b)
	}

	b = Box2D[int]{Vector2[int]{2, 0}, Vector2[int]{3, 3}}
	b.CropToFitIn(a)
	if b != (Box2D[int]{Vector2[int]{2, 1}, Vector2[int]{3, 3}}) {
		t.Error("crop failed", b)
	}

	b = Box2D[int]{Vector2[int]{0, 0}, Vector2[int]{1, 1}}
	b.CropToFitIn(a)
	if b.IsBox() {
		t.Error("no overlap should produce a degenerate box")
	}
}

func TestSegmentBox(t *testing.T) {

	type test struct {
		b  Box2D[int]
		s  int
		bs []Box2D[int]
	}

	tests := []test{
		{Box2D[int]{Vector2[int]{9, 9}, Vector2[int]{0, 0}}, 25,
			[]Box2D[int]{
				{Vector2[int]{0, 0}, Vector2[int]{4, 4}},
				{Vector2[int]{5, 0}, Vector2[int]{9, 4}},
				{Vector2[int]{0, 5}, Vector2[int]{4, 9}},
				{Vector2[int]{5, 5}, Vector2[int]{9, 9}},
			},
		},

		{Box2D[int]{Vector2[int]{2, 2}, Vector2[int]{11, 5}}, 20,
			[]Box2D[int]{
				{Vector2[int]{2, 2}, Vector2[int]{6, 5}},
				{Vector2[int]{7, 2}, Vector2[int]{11, 5}},
			},
		},

		{Box2D[int]{Vector2[int]{2, 2}, Vector2[int]{11, 5}}, 10,
			[]Box2D[int]{
				{Vector2[int]{2, 2}, Vector2[int]{6, 3}},
				{Vector2[int]{7, 2}, Vector2[int]{11, 3}},
				{Vector2[int]{2, 4}, Vector2[int]{6, 5}},
				{Vector2[int]{7, 4}, Vector2[int]{11, 5}},
			},
		},

		{Box2D[int]{Vector2[int]{1, 1}, Vector2[int]{2, 9}}, 7,
			[]Box2D[int]{
				{Vector2[int]{1, 1}, Vector2[int]{2, 3}},
				{Vector2[int]{1, 4}, Vector2[int]{2, 6}},
				{Vector2[int]{1, 7}, Vector2[int]{2, 9}},
			},
		},

		{Box2D[int]{Vector2[int]{1, 1}, Vector2[int]{10, 10}}, 101,
			[]Box2D[int]{
				{Vector2[int]{1, 1}, Vector2[int]{10, 10}},
			},
		},

		{Box2D[int]{Vector2[int]{0, 0}, Vector2[int]{3, 3}}, 22,
			[]Box2D[int]{
				{Vector2[int]{0, 0}, Vector2[int]{3, 3}},
			},
		},
	}

	for i := range tests {
		res := SegmentBox(tests[i].b, tests[i].s)
		t.Log("res", res)
		for j := range tests[i].bs {
			if res[j].P1 != tests[i].bs[j].P1 {
				t.Error(res[j].P1, "!=", tests[i].bs[j].P1)
			}
			if res[j].P2 != tests[i].bs[j].P2 {
				t.Error(res[j].P2, "!=", tests[i].bs[j].P2)
			}
		}
	}
}

func TestBoxSize(t *testing.T) {
	b := Box2D[int]{Vector2[int]{-1, 3}, Vector2[int]{-3, -5}}

	if size := b.Size(); size.X != 2 || size.Y != 8 {
		t.Error(b, size)
	}
}

func TestBoxContains(t *testing.T) {
	tests := []struct {
		x, y int
		p    Vector2[int]
		t    bool
	}{
		{10, 20, Vector2[int]{-1, 0}, false},
		{10, 20, Vector2[int]{5, 22}, false},
		{10, 20, Vector2[int]{5, 12}, true},
		{-10, -20, Vector2[int]{5, 12}, false},
		{-10, -20, Vector2[int]{-5, -12}, false},
	}
	for _, v := range tests {
		b := Box2D[int]{}.New(0, 0, v.x, v.y)
		if b.Contains(v.p) != v.t {
			t.Error(v)
		}
	}
}

func TestBoxFit(t *testing.T) {
	tests := []struct {
		x, y int
		p    Vector2[int]
		r    Vector2[int]
	}{
		{10, 20, Vector2[int]{-1, 0}, Vector2[int]{0, 0}},
		{10, 20, Vector2[int]{5, 22}, Vector2[int]{5, 20}},
		{10, 20, Vector2[int]{5, 12}, Vector2[int]{5, 12}},
		{-10, -20, Vector2[int]{5, 12}, Vector2[int]{0, 0}},
		{-10, -20, Vector2[int]{-5, -12}, Vector2[int]{-5, -12}},
	}
	for i, v := range tests {
		b := Box2D[int]{}.New(0, 0, v.x, v.y)
		b.MakeCanonical()
		if b.Fit(v.p) != v.r {
			t.Error(i, v, b.Fit(v.p))
		}
	}
}

func TestBoxOverlaps(t *testing.T) {
	b1 := Box2D[int]{}.New(0, 0, 3, 1)
	b2 := Box2D[int]{}.New(0, 0, 3, 2)
	b3 := Box2D[int]{}.New(2, 1, 4, 2)

	b4 := Box2D[int]{}.New(1, 5, 4, 6)
	b5 := Box2D[int]{}.New(3, 3, 6, 6)
	b6 := Box2D[int]{}.New(5, 5, 6, 6)

	if !b1.Overlaps(b2) || !b2.Overlaps(b1) {
		t.Error(b1, b2)
	}

	if !b2.Overlaps(b3) || !b3.Overlaps(b2) {
		t.Error(b2, b3)
	}

	if b1.Overlaps(b3) || b3.Overlaps(b1) {
		t.Error(b1, b3)
	}

	if !b4.Overlaps(b5) || !b5.Overlaps(b4) {
		t.Error(b4, b5)
	}

	if !b5.Overlaps(b6) || !b5.Overlaps(b6) {
		t.Error(b5, b6)
	}

	if b4.Overlaps(b6) || b6.Overlaps(b4) {
		t.Error(b4, b6)
	}
}
